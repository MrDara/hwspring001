package com.example.platformSping.Rest.RestController.Response;

import com.example.platformSping.Repository.Model.CategoryModel;

import java.util.List;

public class BookReponseModel {
    private int id;
    private String title;
    private String author;
    private String thumbnail;
    private String description;
    List category;

    public BookReponseModel() {
    }

    public BookReponseModel(int id, String title, String author, String thumbnail, String description, List category) {
        this.id = id;
        this.title = title;
        this.author = author;
        this.thumbnail = thumbnail;
        this.description = description;
        this.category = category;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List getCategory() {
        return category;
    }

    public void setCategory(List category) {
        this.category = category;
    }

    @Override
    public String toString() {
        return "BookReponseModel{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", author='" + author + '\'' +
                ", thumbnail='" + thumbnail + '\'' +
                ", description='" + description + '\'' +
                ", category=" + category +
                '}';
    }
}
