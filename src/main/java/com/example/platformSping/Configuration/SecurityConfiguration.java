package com.example.platformSping.Configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@Configuration
@EnableWebSecurity
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Bean
    public PasswordEncoder passwordEncoder(){
        return new BCryptPasswordEncoder();
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.inMemoryAuthentication()
                .withUser("admin@gmail.com")
                .password(passwordEncoder().encode("12345"))
                .roles("ADMIN");
        auth.inMemoryAuthentication()
                .withUser("user@gmail.com")
                .password(passwordEncoder().encode("12345"))
                .roles("USER");
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.csrf().disable()
                .authorizeRequests()
                .antMatchers(HttpMethod.GET,"/api/v1/book/select").permitAll()
                .antMatchers(HttpMethod.GET,"/api/v1/category").permitAll()
                .antMatchers(HttpMethod.POST,"/api/v1/book/**").hasRole("ADMIN")
                .antMatchers(HttpMethod.POST,"/api/v1/category/**").hasRole("ADMIN")
                .antMatchers(HttpMethod.DELETE,"/api/v1/category/**").hasRole("ADMIN")
                .antMatchers(HttpMethod.DELETE,"/api/v1/book/**").hasRole("ADMIN")
                .antMatchers(HttpMethod.PUT,"/api/v1/category/**").hasRole("ADMIN")
                .antMatchers(HttpMethod.PUT,"/api/v1/book/**").hasRole("ADMIN")
                .anyRequest()
                .authenticated()
                .and()
                .httpBasic();

    }
}
