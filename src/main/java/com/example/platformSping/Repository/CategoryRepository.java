package com.example.platformSping.Repository;

import com.example.platformSping.BookProviderSQl.CategoryProvider;
import com.example.platformSping.Repository.Model.CategoryModel;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CategoryRepository {
    @InsertProvider(type = CategoryProvider.class,method = "InsertCategory")
    boolean insert(CategoryModel categoryModel);
    @SelectProvider(type = CategoryProvider.class,method = "SelectCategory")
    @Results({
            @Result(property = "id",column = "category_id")
    })
    List<CategoryModel> select();

    @DeleteProvider(type = CategoryProvider.class,method = "DeleteCategory")
    int DeletedCategoryById(int id);

    @UpdateProvider(type = CategoryProvider.class,method = "UpdateCategory")
    int UpdateCategory(int id,CategoryModel categoryModel);
}
