package com.example.platformSping.Service;

import com.example.platformSping.Repository.Model.CategoryModel;

import java.util.List;

public interface CategoryService {
    CategoryModel insert(CategoryModel categoryModel);
    List<CategoryModel> select();
    int DeletedCategoryById(int id);
    int UpdateCategory(int id,CategoryModel categoryModel);
}
