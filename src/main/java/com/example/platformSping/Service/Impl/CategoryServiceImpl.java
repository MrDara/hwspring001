package com.example.platformSping.Service.Impl;

import com.example.platformSping.Repository.CategoryRepository;
import com.example.platformSping.Repository.Model.CategoryModel;
import com.example.platformSping.Service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CategoryServiceImpl implements CategoryService {

    private CategoryRepository categoryRepository ;

    @Autowired
    public void setCategoryRepository(CategoryRepository categoryRepository) {
        this.categoryRepository = categoryRepository;
    }


    @Override
    public CategoryModel insert(CategoryModel categoryModel) {
        boolean isInserted  = categoryRepository.insert(categoryModel);
        if(isInserted){
            return categoryModel;
        }else{
            return null;
        }
    }

    @Override
    public List<CategoryModel> select() {
        return categoryRepository.select();
    }

    @Override
    public int DeletedCategoryById(int id) {
        return categoryRepository.DeletedCategoryById(id);
    }

    @Override
    public int UpdateCategory(int id,CategoryModel categoryModel) {
        return categoryRepository.UpdateCategory(id,categoryModel);
    }
}
