package com.example.platformSping.BookProviderSQl;

import com.example.platformSping.Repository.Model.CategoryModel;
import org.apache.ibatis.jdbc.SQL;

public class CategoryProvider {
    public String InsertCategory(){
        return  new SQL(){{
            INSERT_INTO("tb_categories");
            VALUES("title","#{title}");
        }}.toString();
    }
    public String SelectCategory(){
        return  new SQL(){{
            SELECT("*");
            FROM("tb_categories");
        }}.toString();
    }
    public String DeleteCategory(int id){
        return new SQL(){{
            DELETE_FROM("tb_categories");
            WHERE("category_id = #{id}");
        }}.toString();
    }
    public String UpdateCategory(int id,CategoryModel categoryModel){
        return new SQL(){{
            UPDATE("tb_categories SET title = #{categoryModel.title}  Where category_id = '"+id+"'");
        }}.toString();
    }
}
