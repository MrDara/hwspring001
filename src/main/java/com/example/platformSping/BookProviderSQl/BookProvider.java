package com.example.platformSping.BookProviderSQl;

import com.example.platformSping.Repository.Model.BookModel;
import org.apache.ibatis.jdbc.SQL;

public class BookProvider {
    public String insertBookSql(){
        return new SQL(){{
            /// Define SQL statement
            INSERT_INTO("tb_books");
            VALUES("title","#{title}");
            VALUES("author","#{author}");
            VALUES("thumbnail","#{thumbnail}");
            VALUES("description","#{description}");
            VALUES("category_id","#{category.id}");
        }}.toString();
    }

    public String SelectAllBookSql(){
        return new SQL(){{
            SELECT("*");
            FROM("tb_books");
        }}.toString();
    }



    public String selectCategoryByIdSql(int id){
        return new SQL(){{
            SELECT("*");
            FROM("tb_books b");
            INNER_JOIN("tb_categories c ON b.category_id = c.category_id");
            WHERE("c.category_id = #{id}");
        }}.toString();
    }


    public String DeleteBookByIdSql(int id){
        return new SQL(){{
            DELETE_FROM("tb_books");
            WHERE("id = #{id}");
        }}.toString();
    }


    public String UpdetaBookByIdSql(int id, BookModel bookModel){
        return new SQL(){{
            UPDATE("tb_books SET title = #{bookModel.title} , author = #{bookModel.author}, thumbnail = #{bookModel.thumbnail}, description = #{bookModel.description}, category_id = #{bookModel.category.id}");
            WHERE("id = '"+id+"'");
        }}.toString();
    }


    public String SelectFilterByBookTitle(String title){
        return new SQL(){{
            SELECT("*");
            FROM("tb_books");
            if(title !=null || !title.equals("")){
                WHERE("title like '%"+title+"%'");
            }else{
                WHERE();
            }
        }}.toString();
    }

    public String FindOneByID(int id){
        return new SQL(){{
            SELECT("*");
            FROM("tb_books");
            WHERE("id = #{id}");
        }}.toString();
    }

}
